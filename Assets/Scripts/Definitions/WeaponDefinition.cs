﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Object/Weapon Definition", order = 110)]
public class WeaponDefinition : ScriptableObject
{
    [SerializeField]
    string itemID = "Must be Unique";

    [SerializeField]
    string displayName = "default";

    [SerializeField]
    [Tooltip("Overrides the weapon to use a beam instead of projectile")]
    bool beamType = false;

    [SerializeField]
    [Tooltip("Makes beam fire constantly drain energy cost per second")]
    bool continuousBeam = false; 

    [SerializeField]
    float energy;

    [SerializeField]
    float energyCost;

    [SerializeField]
    float energyRegen;

    [SerializeField]
    FireModes firemode = FireModes.automatic;

    [SerializeField]
    float fireRate;

    [SerializeField]
    float fireDelay;

    [SerializeField]
    int burstCount;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
