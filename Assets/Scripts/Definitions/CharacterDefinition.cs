﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Object/Character Definition", order = 100)]
public class CharacterDefinition : ScriptableObject
{
    [SerializeField]
    string itemID = "Must be Unique";

    [SerializeField]
    string displayName = "Default";

    [SerializeField]
    float health;

    [SerializeField]
    float energon;

    [SerializeField]
    float energy;



    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
