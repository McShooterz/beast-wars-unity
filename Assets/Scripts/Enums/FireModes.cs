﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FireModes
{
    automatic,
    single,
    burst
}
